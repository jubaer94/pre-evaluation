package preevaluation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PreEvaluationWarApplication {


	public static void main(String[] args) {
		SpringApplication.run(PreEvaluationWarApplication.class, args);
	}

}
