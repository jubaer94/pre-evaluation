package preevaluation.registration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import preevaluation.registration.model.User;
import preevaluation.registration.repository.UserRepository;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
     public List<User> getAllUsers()
     {
         List<User> users = userRepository.findAll();
         return  users;
     }
    public User getUserById(int id)
    {

        return userRepository.getById(id);
     }
    public User getUserByName(String name)
    {

        return userRepository.getByName(name);
    }

     public User saveUser(User user)
     {
         User user1 = userRepository.save(user);
         return user1;
     }
     public User updateUser(User user)
     {
         return userRepository.save(user);
     }

     public void deleteUserById(int id)
     {
         userRepository.deleteById(id);
     }

     public void deleteAllUsers()
     {
         userRepository.deleteAll();
     }



}
