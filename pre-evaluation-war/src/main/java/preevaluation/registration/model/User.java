package preevaluation.registration.model;

import com.sun.istack.NotNull;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.Random;

@Entity
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private int id;
    @Column(name = "Reg_id" , unique = true)
    private String Reg_id;
    @NotNull
    private String first_name;
    @NotNull
    private String last_name;
    @DateTimeFormat
    private Date reg_date;
    @DateTimeFormat
    private Date date_of_birth;
    private int age;
    private String gender;
    @NotNull
    private long nid;
    private String address;
    @NotNull
    private long contact_no;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name="photo", nullable=true)
    private byte[] photo;
    @CreationTimestamp
    private Date created_on;
    private String academic_qualifications;
    @NotNull
    @Column(name = "name" , nullable = false ,unique = true)
    private String name;
    @Column(name = "institution_id")
    private String institution_id;
    @Column(unique = true , nullable = false , name = "email")
    private String email;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReg_id() {
        return Reg_id;
    }

    public void setReg_id(String reg_id) {
        Reg_id = reg_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getReg_date() {
        return reg_date;
    }

    public void setReg_date(Date reg_date) {
        this.reg_date = reg_date;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getNID() {
        return nid;
    }

    public void setNID(long nid) {
        this.nid = nid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getContact_no() {
        return contact_no;
    }

    public void setContact_no(long contact_no) {
        this.contact_no = contact_no;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }

    public String getAcademic_qualifications() {
        return academic_qualifications;
    }

    public void setAcademic_qualifications(String academic_qulifications) {
        this.academic_qualifications = academic_qulifications;
    }

    public String getInstitution_id() {
        return institution_id;
    }

    public void setInstitution_id(String institution_id) {
        this.institution_id = institution_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
