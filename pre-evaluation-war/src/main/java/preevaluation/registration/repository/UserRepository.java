package preevaluation.registration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import preevaluation.registration.model.User;

@Repository
public interface UserRepository extends JpaRepository<User ,Integer> {
    User getByName(String name);

}
